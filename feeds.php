<?php
// phpinfo(); exit;

require "vendor/autoload.php";

//$client = new GuzzleHttp\Client();

$feeds_urls = [
    'http://feeds.arstechnica.com/arstechnica/everything',
    'http://www.reddit.com/.rss',
    'http://feeds.feedburner.com/boingboing/iBag',
    'http://www.theverge.com/rss/index.xml',
    'http://feeds.wired.com/wired/index',
    'http://rss.nytimes.com/services/xml/rss/nyt/Technology.xml',
    'http://feeds.feedburner.com/Techcrunch',
    'http://geekdad.com/feed/',
    'http://photography.tutsplus.com/posts.atom',
    'http://webdesign.tutsplus.com/posts.atom',
    // 'http://www.timesfreepress.com/rss/headlines/cartoons/',
    'http://code.tutsplus.com/categories/wordpress.atom',
    'http://rss.slashdot.org/Slashdot/slashdot',
    'http://feeds.gawker.com/lifehacker/full',
    'http://www.engadget.com/rss.xml',
    'http://feeds.feedburner.com/techdirt/feed',
    'http://feeds.gawker.com/gizmodo/full',
    'http://www.smashingmagazine.com/feed/',
    'http://code.tutsplus.com/posts.atom',
    'http://design.tutsplus.com/posts.atom',
    'http://www.smashingmagazine.com/feed/',
    'http://design.tutsplus.com/posts.atom',
    'http://recode.net/feed/',
    // //'http://iso.500px.com/feed/',
    'http://feeds.feedburner.com/BoredPanda',
    'http://www.huffingtonpost.com/news/parents-dads/feed=rss2',
    'http://www.huffingtonpost.com/feeds/verticals/parents/index.xml',
    // 'http://babyhoward.weebly.com/2/feed',
    'http://www.androidpolice.com/feed/',
    // 'http://readthinkrepeat.wordpress.com/feed/',
    // 'http://baby.machiavello.net/feed/',
    'http://feeds.bbci.co.uk/sport/0/tennis/rss.xml',
    // 'http://sports.yahoo.com/tennis/rss.xml',
    'http://syndication.dp.discovery.com/rss/Discovery+News',
    /*
    'http://feeds.feedburner.com/uxbooth',
    'http://youvsthem.wordpress.com/feed/',
    'http://youarenotsosmart.com/feed/',
    'http://www.uxmatters.com/index.xml',
    'http://feeds.feedburner.com/SixRevisions',
    */
];

$feeds = [];

/* STRAIGHT XML
 foreach ($feeds_urls as $url) {
    $response = $client->get($url);
    $xml = simplexml_load_string($response->getBody());
    $feeds[] = $xml;
}
*/

/* SIMPLE PIE
foreach ($feeds_urls as $url) {
    $feed = new SimplePie();
    $feed->set_feed_url($url);
    $feed->init();

    $articles = [];
    foreach ($feed->get_items() as $item) {
        $articles[] = [
            'headline' => $item->get_title(),
            'link' => $item->get_permalink()
        ];
    }

    $feeds[] = [
        'title' => $feed->get_title(),
        'link' => $feed->get_link(),
        'icon' => $feed->get_image_url(),
        'articles' => $articles
    ];
}
*/

/*
 * picoFeed
 */

$reader = new \PicoFeed\Reader\Reader();
foreach ($feeds_urls as $url) {

    // Get a resource
    $resource = $reader->download($url);

    // Return the right parser for the feed
    $parser = $reader->getParser(
        $resource->getUrl(),
        $resource->getContent(),
        $resource->getEncoding()
    );

    // Return a feed object
    $feed = $parser->execute();

    $articles = [];
    foreach ($feed->getItems() as $item) {
        $articles[] = [
            'headline' => $item->getTitle(),
            'link' => $item->getUrl()
        ];
    }

    $feeds[] = [
        'title' => $feed->getTitle(),
        'link' => $feed->getSiteUrl(),
        'icon' => $feed->getIcon(),
        'articles' => $articles
    ];

}


header('Content-Type: application/json');
print json_encode($feeds);
