// Set up Vue
var Vue = require('../bower_components/vue/dist/vue');
Vue.use(require('../bower_components/vue-resource/dist/vue-resource'));

// Install Masonry
var Masonry = require('masonry-layout');

// Start the app
var app = new Vue({

    el: '#app',

    components: {
        feed: require('./components/feed.vue')
    },

    data: {
        feeds: []
    },

    methods: {
    },

    ready: function() {

        this.$http.get('feeds.php', function(data, status, request){
            this.$set('feeds', data);
        })

        //var $container = $('.masonry-container');
        //$container.imagesLoaded( function () {
        //    $container.masonry({
        //        columnWidth: '.panel',
        //        itemSelector: '.panel'
        //    });
        //});

        var msnry = new Masonry( '#app', {
            columnWidth: '.feeds',
            itemSelector: '.feeds'
        });
    }
});
